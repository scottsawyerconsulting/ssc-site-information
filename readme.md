Site Information 
=================

This is a Drupal 7 module, the basic idea is that many site owners need some standard attributes on their site:

+  Phone number
+  Address
+  Email ( maybe different than the administration email in Drupal )
+  Facebook
+  LinkedIn
+  Twitter
+  YouTube
+  Google Plus
+  Google Maps

Motivation
----------
Often site builders and themers will either hard code, use theme_settings, or put this information in blocks or other content.  
This makes it hard to:
1.  Switch themes, 
2.  maintain consistency.

Solution
--------
This module adds these settings to the site settings form, usually located at admin/config/system/site-information

These settings can be retrieved anywhere you can use variable_get, but this module provides a CTools plugin which allows the site builder or practically any user, assuming they have CTools installed, to easily display the settings.

If you use Panels or any variant, like Panels Everywhere, when you add content, select Page elements, Site Information.  You can choose which of the settings you want to be displayed.  This gives you nice drag and drop ordering, conditional display, you can set CSS attributes on the container, and do anything you can with Panels content.

Limitations
-----------
Currently, the output formatting is based on specific client needs, so I have not spent too much time making the output flexible or even configurable from the UI.  But the usage is pretty handy.

For instance, the Phone Number field.  When a phone number is added in the form 555-555-5555, it will create a tel: link with the phone number.  It does a simple str_replace to remove the dashes.  If you use parens, it does not work.  

The Email field creates a mailto: link.  

The social media links just create a link with the link text as the name of the social network.  

It does provide decent class structures so targeting with CSS and JS should be plenty efficient, so most formatting issues can be overcome easily.

TODO
----
+ I would like to make the list of fields more flexible, maybe add a UI to create new fields.
+ The address fields are separate inputs, and they are rendered separately.  This is not too useful and an oversight when I started.  It would be a good idea to render them together.
+ The settings form should have a wrapper, fieldset, maybe collapsable.  
+ More options for adjusting the rendering would be great.  For instance, allowing the user to upload an image for the social media sites.
But hey, I have to sleep sometime.

http://www.scottsawyerconsulting.com



<?php
  /**
   * CTools plugin for SSC Information
   */
$plugin = array(
  'single' => TRUE,
  'title' => t('Site Information'),
  'description' => t('Displays Site Information '),
  'category' => t('Page elements'),
  'edit form' => 'ssc_site_information_pane_edit_form',
  'render callback' => 'ssc_site_information_pane_render',
  'admin info' => 'ssc_site_information_pane_admin_info',
  'defaults' => array(
    'site_information_composite_fields' => '',
    'site_information_composite' => '0',
  	'site_information' => '',
    'site_information_wrapper' => 'div',
    'site_information_field' => 'p',
  	'override_title' => FALSE,
  	'override_title_text' => '',
  	'override_title_heading' => 'h2'
  	),
  'all contexts' => TRUE,
);
/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */
function ssc_site_information_pane_render($subtype, $conf, $args) {
  //var_dump($conf);
  $fields = array(); // This will be the container for our fields.
  $display_class = '';
  $wrapper_start = '';
  $wrapper_end = '';
  $field_html_start = '';
  $field_html_end = '';
  $markup = '';
  $title = '';  
  $wrapper_setting = $conf['site_information_wrapper'];
  $field_setting = $conf['site_information_field'];
  $site_fields = ssc_site_fields();
  if ($conf['site_information_composite'] == 0) {
    // Single field selected.
    $display_setting = $conf['site_information'];
    $title = $site_fields[$display_setting]['form_fields']['#title'];
    $display_class = $display_setting;
    $fields[] = $display_setting;
  }
  else {
    // Multiple fields are selected.
    $composite_fields = $conf['site_information_composite_fields'];
    $display_class = 'composite ';
    foreach ($composite_fields as $composite_value => $label) {
      if ($label) {
        $display_class .= $label . ''; 
        $fields[] = $label;
      }
    }   
    $title = 'Composite Fields'; 
  }

  foreach ($fields as $key => $field) {
    $content = filter_xss_admin(variable_get($field));
    if(!empty($content)) {
      $markup .= ssc_site_information_field_build($field, $content);
      if ($field_setting != '_none_') {
        $field_html_start = '<' . $field_setting . ' class="site-setting site-setting-field ' . $display_class . '">';
        $field_html_end = '</' . $field_setting . '>';
      }
    }
  }
  if ($wrapper_setting != '_none_') {
    $wrapper_start = '<' . $wrapper_setting . ' class="site-setting site-setting-wrapper ' . $display_class . '">';
    $wrapper_end = '</' . $wrapper_setting . '>';
  }
		
  $block = new stdClass();
  $block->title = $title;
  $block->content = $wrapper_start . $field_html_start . $markup . $field_html_end . $wrapper_end;
  return $block;
}

/**
 * Format the individual items.
 * @param $field = field key
 * @param $content = field content
 */
function ssc_site_information_field_build($field, $content) {
  $site_fields = ssc_site_fields();
  switch($field){
    case 'site_phone':
      $value = '<a href="tel:' . str_replace('-', '', $content) . '">' . $content . '</a> ';
      break;
    case 'site_email':
      $value = '<a href="mailto:' . $content . '">' . $content . '</a> ';
      break;
    case 'site_facebook':
      $value = '<a href="' . $content . '" rel="' . $field . '">' . $site_fields[$field]['form_fields']['#title'] . '</a> ';
      break;
    case 'site_twitter':
      $value = '<a href="' . $content . '" rel="' . $field . '">' . $site_fields[$field]['form_fields']['#title'] . '</a> ';  
      break;
    case 'site_linkedin':
      $value = '<a href="' . $content . '" rel="' . $field . '">' . $site_fields[$field]['form_fields']['#title'] . '</a> ';  
      break;
    case 'site_google':
      $value = '<a href="' . $content . '" rel="' . $field . '">' . $site_fields[$field]['form_fields']['#title'] . '</a> ';              
      break;
    case 'site_youtube':
      $value = '<a href="' . $content . '" rel="' . $field . '">' . $site_fields[$field]['form_fields']['#title'] . '</a> ';
      break;
    default: 
      $value = $content . ' ';    
  }
  return $value;
}
/**
 * CTools edit form
 */
function ssc_site_information_pane_edit_form($form, &$form_state){
  // Include the ctools dependency.
  ctools_include('dependent');
  $conf = $form_state['conf'];
  $site_fields = ssc_site_fields();
  $options = array();
  foreach($site_fields as $option) {
    $options[$option['machine_name']] = $option['form_fields']['#title']; 
  }
  /**
   * Add a checkbox to make a composite field.
   * This is good for making address fields.
   */
  $form['site_information_composite'] = array(
    '#type' => 'radios',
    '#title' => t('Make a composite field.'),
    '#description' => t('This is good for making a single element of several fields, such as an address.'),
    '#default_value' => $conf['site_information_composite'] ?  $conf['site_information_composite'] : '0',
    '#options' => array(
      '0' => 'Single Item',
      '1' => 'Multiple Items',
    ),
  );
  $form['site_information'] = array(
  	'#type' => 'select',
  	'#title' => t('Select the site setting you want to render.'),
  	'#options' => $options,
  	'#default_value' => $conf['site_information'],
  	'#description' => t('Select the site setting to render.'),
    '#states' => array(
      'visible' => array(
        ':input[name="site_information_composite"]' => array('value' => '0'),
      ),
    ),
  );
  /**
   * Add dependent checkboxes of available options to combine to a single field.
   */
  $form['site_information_composite_fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Available fields.'),
    '#description' => t('Choose the properties to combine into one field.'),
    '#states' => array(
      'visible' => array(
        ':input[name="site_information_composite"]' => array('value' => '1'),
        ),
      ),
    '#options' => $options,
    '#default_value' => $conf['site_information_composite_fields'],
  );
  $form['site_information_wrapper'] = array(
    '#type' => 'select',
    '#title' => t('Select the HTML wrapper for this setting.'),
    '#options' => array(
      '_none_' => t('No markup'),
      'span' => t('span'),
      'div' => t('div'),
      'address' => t('address'),
    ),
    '#default_value' => $conf['site_information_wrapper'],
    '#description' => t('Select the wrapper HTML. NOTE: if you choose address, you should use the Composite option, and choose "No markup" or "Span" for the field HTML.'),
  );
  $form['site_information_field'] = array(
    '#type' => 'select',
    '#title' => t('Select the field HTML for this setting.'),
    '#options' => array(
      '_none_' => t('No markup'),
      'span' => t('span'),
      'p' => t('p'),
      ),
    '#default_value' => $conf['site_information_field'],
    '#description' => t('NOTE: choosing "p" if you choose "span" for the Wrapper will result in invalid HTML.'),
    );
  return $form;

}
/**
 * CTools submit handler.
 */
function ssc_site_information_pane_edit_form_submit($form, &$form_state){
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}
/**
 * Ctools admin info.
 */
function ssc_site_information_pane_admin_info($subtype, $conf, $context = NULL) {
	$site_fields = ssc_site_fields();
  $context = new stdClass();
  $context->data = new stdClass();
  $context->data->description = $site_fields[$conf['site_information']]['form_fields']['#title'];
  $block = ssc_site_information_pane_render($subtype, $conf, array("Site Information"), $context);
  return $block;
}
